export const TO_LOGIN = 'members/TO_LOGIN';
export const TO_REGISTER = 'members/TO_REGISTER';
export const MobileVerification = 'members/MobileVerification';
export const ResetRegistration = 'members/ResetRegistration';
export const TO_LOGOUT = 'members/TO_LOGOUT';
