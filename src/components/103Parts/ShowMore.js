import React, { Component } from 'react';

class ShowMore extends Component {
    render() {
        return (
            <div className="wrapper-show-more">
                <div className="show-more">Show More</div>
            </div>
        )
    }
}

export default ShowMore;
